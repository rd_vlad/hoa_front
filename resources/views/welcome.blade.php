@extends('layouts.template')

@section('content')
<div id="home">
    <section class="principal-container">
        <video autoplay muted loop>
            <source src="{{asset('assets/video/delmar_home_video.mp4')}}" type="video/mp4" />
        </video>
    </section>
    @include ('layouts.covidtag')
    <section class="welcome-container">
        <div class="two-cols">
            <div>
                <div class="img-container">
                    <img src="{{asset('assets/img/home/bugambilia.jpg')}}" alt="">
                    <img src="{{asset('assets/img/home/cancha.jpg')}}" alt="">
                </div>
            </div>
            <div>
                <div class="custom-title">
                    <span>
                        Welcome
                    </span>
                </div>
                <div class="text-container">
                    <h3>The HOA Team</h3>
                    <p>
                        Ensure that all collective rights and interests of homeowners
                        are respected and preserved. Our core principles are to provide
                        services to residents & homeowners, protect property value
                        and meet the established expectations of homeowners.
                    </p>
                    <p>
                        By maintaining a standard of appearance of the properties &
                        common areas, abiding and reinforcing regulations and
                        guidelines as well as upkeeping the community standards, the
                        HOA supports stable property values for all homeowners.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="villas-description">
        <div class="background-image">
            <div class="empty-container"></div>
            <div class="bottom-container">
                <div class="background-white-container">
                    <div class="dresciption-container">
                        <h2>
                            Villas del Mar
                        </h2>
                        <p>
                            Founded in 1994, Villas Del Mar is now a landmark with over 200 constructed homes featuring classic Palmilla style, mediterranean style columns and lush landscaping. First breaking ground in a stretch beachfront next to the Hotel Palmilla, the first villas were built to be promptly followed by carved homes in the adjacent hills bringing innovative and unbelievable construction and techniques for the late '90s Cabo.
                        </p>
                        <div class="custom-button-style">
                            <a href="">
                                View Map
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection