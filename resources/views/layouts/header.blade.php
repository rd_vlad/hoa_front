<header id=header>
    <div class="hoa-header">
        <div class="header-container">
            <div class="header-navbar-container">
                <div class="header-social-media-weather">
                    <div class="header-weather">
                        <a class="dropdown-toggle icon-container" href="#" id="btnWeather" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{asset('assets/img/icons/white/weather.png')}}" alt="HOA Weather White Icon">
                            <img src="{{asset('assets/img/icons/brown/weather.png')}}" alt="HOA Weather Color Icon">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="btnWeather">
                            <!-- www.tutiempo.net - Ancho:225px - Alto:434px -->
                            <div id="TT_JfDsGZeYolKj4GDKjAzEEkkkEsnKBpK53I3oG"></div>
                            <script type="text/javascript" src="https://www.tutiempo.net/s-widget/l_JfDsGZeYolKj4GDKjAzEEkkkEsnKBpK53I3oG"></script>
                        </div>
                    </div>
                    <div class="header-social-media">
                        <span class="follow-social-media">
                            <span>Follow Us</span>
                        </span>
                        <span class="social-media-icons">
                            <span class="icon-container">
                                <a href="https://www.facebook.com/DelMarLosCabos/" target="_blank">
                                    <img src="{{asset('assets/img/icons/white/facebook.png')}}" alt="HOA Facebook White Icon">
                                    <img src="{{asset('assets/img/icons/brown/facebook.png')}}" alt="HOA Facebook Color Icon">
                                </a>
                            </span>
                            <span class="icon-container">
                                <a href="https://www.instagram.com/delmarloscabos/" target="_blank">
                                    <img src="{{asset('assets/img/icons/white/instagram.png')}}" alt="HOA Instagram White Icon">
                                    <img src="{{asset('assets/img/icons/brown/instagram.png')}}" alt="HOA Instagram Color Icon">
                                </a>
                            </span>
                        </span>
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg navbar-dark">

                    <a class="navbar-brand icon-container" href="/">
                        <img src="{{asset('assets/img/logos/logo-white.png')}}" target="blank" alt="HOA White Logo">
                        <img src="{{asset('assets/img/logos/logo-color.png')}}" target="blank" alt="HOA Logo">
                    </a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                            <i class="fas fa-bars"></i>
                            <i class="fas fa-times"></i>
                        </span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto">

                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Homeowners
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">CC&R´S</a>
                                    <a class="dropdown-item" href="#">Budgets & Fee</a>
                                    <a class="dropdown-item" href="#">Community Guidelines</a>
                                    <a class="dropdown-item" href="#">Insurance</a>
                                    <a class="dropdown-item" href="#">Renovations/Construction</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                    <a class="dropdown-item" href="#">About us</a>
                                    <a class="dropdown-item" href="#">Community Maps</a>
                                    <a class="dropdown-item" href="#">FAQ´s</a>
                                    <a class="dropdown-item" href="#">Gallery</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Directory</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                    <a class="dropdown-item" href="#">Homeowner Directory</a>
                                    <a class="dropdown-item" href="#">Board Members</a>
                                    <a class="dropdown-item" href="#">Architectural Review Committee</a>
                                    <a class="dropdown-item" href="#">HOA Team</a>
                                    <a class="dropdown-item" href="#">Pet Directory</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact</a>
                            </li>
                        </ul>

                    </div>
                </nav>
                <div class="header-user-cam-buttons">
                    <div class="header-user-buttons">
                        <span class="user-icon icon-container">
                            <img src="{{asset('assets/img/icons/white/profile.png')}}" alt="HOA User White Icon">
                            <img src="{{asset('assets/img/icons/brown/profile.png')}}" alt="HOA User Brown Icon">
                        </span>
                        <span class="user-buttons">
                            <span>
                                <a href="">
                                    Login
                                </a>
                            </span> |
                            <span>
                                <a href="">
                                    Register
                                </a>
                            </span>
                        </span>
                    </div>
                    <div class="header-camera-btn icon-container">
                        <a href="#">
                            <img src="{{'assets/img/icons/white/cam.png'}}" alt="HOA User White Cam">
                            <img src="{{'assets/img/icons/brown/cam.png'}}" alt="HOA User Brown Cam">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>