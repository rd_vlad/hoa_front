<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    <div id="app">
        
        @include("layouts.header")

        @yield("content")
        @include("layouts.footer")

        @include("layouts.modals")

        @include("layouts.bottomscripts")
        @yield('scripts')
    </div>
</body>
</html>