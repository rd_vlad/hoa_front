<footer id="footer">
    <div class="footer-container container-fluid">
        <section class="footer-top-container">
            <div class="footer-cols-container">
                <div class="footer-address-hours">
                    <div class="footer-address">
                        <span class="footer-address-title footer-title">
                            Visit Us
                        </span>
                        <span class="footer-address-text footer-text">
                            Carr. Transpeninsular Km 27.5, Tourist Corridor, <br>
                            San José del Cabo, B.C.S.
                        </span>
                    </div>
                    <div class="footer-hours">
                        <span class="footer-hours-title footer-title">
                            Office Hours
                        </span>
                        <span class="footer-hours-text footer-text">
                            Monday - Sunday: 8am - 6pm <br>
                            Tel. (624) 105 8710
                        </span>
                    </div>
                </div>
                <div class="footer-social-media">
                    <span class="footer-social-media-title footer-title">
                        Follow Us
                    </span>
                    <span class="footer-social-media-subtitle footer-text footer-subtitle">
                        OUR SOCIAL MEDIA CHANNELS
                    </span>
                    <span class="footer-social-media-icons">
                        <span class="icon-container">
                            <a href="https://www.facebook.com/DelMarLosCabos/" target="_blank">
                                <img src="{{asset('assets/img/icons/white/facebook.png')}}" alt="HOA Facebook Footer Icon">
                            </a>
                        </span>
                        <span class="icon-container">
                            <a href="https://www.instagram.com/delmarloscabos/" target="_blank">
                                <img src="{{asset('assets/img/icons/white/instagram.png')}}" alt="HOA Instagram Footer Icon">
                            </a>
                        </span>
                    </span>
                    <span class="footer-social-media-text footer-text">
                        Follow us on social media to keep up to date with latest news.
                    </span>
                </div>
                <div class="footer-newsletter">
                    <span class="footer-newsletter-title footer-title">
                        Sign up for our newsletter
                    </span>
                    <span class="footer-social-media-subtitle footer-text footer-subtitle">
                        SIGN UP FOR RECENT NEWS
                    </span>
                    <form method="POST" action="">
                        <input name="email" type="text" class="form-control" id="emailNewsletter" placeholder="YOUR EMAIL ADDRESS">
                        <button type="submit" class="btn">-></button>
                    </form>
                    <p class="footer-newsletter-text footer-text">By submitting this form, I agree to having my personal and contact information processed and used for the purpose of marketing communications.</p>
                    <p class="footer-newsletter-text footer-text">You can unsubscribe from these at any time just click on the “unsubscribe” link included in each newsletter.</p>
                </div>
            </div>
        </section>
        <section class="footer-bottom-container">
            <div>
                <span>
                    <a href="/">
                        <img src="{{asset('assets/img/icons/white/mktideas.png')}}" alt="">
                    </a>
                </span>
            </div>
        </section>
    </div>
</footer>